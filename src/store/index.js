import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'


Vue.use(Vuex)

export default new Vuex.Store({
  state: {
 //Variable "globales".
  compras: 0,
  poblacion: 0,
  area: 0,
  paises: [],
  likes: 0,
  agregaciones: [],
  agregaciones2: [],
  contador: 0,
  ultimoDato: '',
  paisesConLike: [],
  paisLike : '',
  estadoLike: [],
  index : 0
  },
  getters: {
  /*Este Getter calcula una nuevo dato a partir
  de las variables establecidas.*/
  promedio(state) {
  if (state.compras === 0) {
  return 0
  }
  return parseInt(state.poblacion / state.compras)
  },



  ultimasAgregaciones(state){
    if (state.contador < 6){
      return state.agregaciones2

    }else if(state.contador >= 5){
      state.contador = 0
      state.ultimoDato = state.agregaciones2[5]
      state.agregaciones2 = []
      state.agregaciones2.push(state.ultimoDato)
      state.contador = state.contador +1 
      return state.agregaciones2
      
    }
    
  }

  },
  mutations: {
  nuevaCompra(state, precio) {
  state.compras = state.compras + 1
  state.poblacion = state.poblacion + precio

  
  },
  agregarArea(state, area) {
  state.area = state.area + area
  },
  getPaises(state, paises) {
  state.paises = paises
  
  },
  setLikes(state, like){
  if (like){

  state.likes++
  
  } else {
    state.likes--
  }
  },




  //MUTACION PARA OBTENER LOS NOMBRES DE LOS PAISES Y LUEGO REALIZAR AGREGACION
  getName(state,agrega){
    state.agregaciones.push(agrega)
    state.agregaciones2.push(agrega)
    state.contador = state.contador +1 
  },



  //MUTACION PARA OBTENER EL PAIS CON LIKE
  getLikes(state,nombrePais){
    state.paisLike = nombrePais
    var i = state.paisesConLike.indexOf(nombrePais);
    if (i !== -1 ){
      state.paisesConLike.splice(i,1)
    }else{
      state.paisesConLike.push(state.paisLike)
    }

  },


//MUTACION PARA RESETEAR TODOS LOS DATOS
  reset(state){
    state.compras = 0
    state.poblacion = 0
    state.promedio = 0
    state.area = 0
    state.setLikes = 0
    state.agregaciones = []
    state.agregaciones2 =[]
    state.contador = 0
    state.estadoLike =[]
    state.paisesConLike = []
  },



  //MUTACIONES PARA ESTABLECER LIKES CON STATE
  estadodelLike(state,estado){
    state.estadoLike[state.index] = !estado;
    },

  indexLike(state, index){
    state.index = index
    },

  },
actions: {
  /*Se crea un action con el objetivo de obtener los países(ConsumirlaAPI),
  mediante un GET*/
 //Metodo Asincronico.
  async getAllCountries(context) {
    return await axios.get('https://restcountries.eu/rest/v2/regionalbloc/usan').then(response => {
      let paises = response.data 
      context.commit('getPaises', paises) })
  }
  }
})
