import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

  const router = new Router({
  
  mode: 'history',
  routes: [   
    
    {
    path: '/',
    component: () => import('../views/Vuex')
  },
  {
    path: '/home',
    component: () => import('../views/Home')
  },
  {
    path: '/about',
    component: () => import('../views/About')
  },
  {
  path: '*',
  redirect: '/'
  }
]
  })

export default router
